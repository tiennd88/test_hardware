import json
import urllib2
import sys
import os
import platform


hostname = "192.168.1.1"

if platform.system().lower()=="linux":
	response = os.system("ping -c 3 -W 5 " + hostname + '>' + os.devnull)

elif platform.system().lower()=="windows":
	response = os.system("ping -n 3 -w 5 " + hostname + '>' + os.devnull)

if response != 0:
	print "---------------ERROR ------------"
	print "PING LAN1 FAILED"
	sys.exit()

url = 'http://192.168.1.1/ubus/csmpub.test_hardware'
data = json.dumps({'jsonrpc': '2.0', 'id': '0', 'method': 'call', 'params': ( '00000000000000000000000000000000', 'csmpub', 'test_hardware', {})} )
req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
f = urllib2.urlopen(req)
response = f.read()

response = json.loads(response)


result = response["result"][1]
error = result["error"]
summary = result["summary"]

try:
	SIG = result["SIG"]
except :
	SIG="SIG"


if error != None:
	print "---------------ERROR ------------"
	print error


print "---------------SUMMARY------------"
print summary

f.close()

try:
	os.mkdir("logs")
except OSError:
	print

	
os.chdir("logs")

report_name = 'report_log_' + SIG + ".txt"
report_file = open(report_name, "w")
report_file.write("---------------ERROR ------------")
report_file.write("%s" % error )
report_file.write("---------------SUMMARY------------")
report_file.write("%s" % summary )
report_file.close()


